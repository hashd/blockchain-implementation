package com.danduprolu.kiran.demo.blockchain.web.controllers;

import com.danduprolu.kiran.demo.blockchain.components.BlockchainManager;
import com.danduprolu.kiran.demo.blockchain.models.Block;
import com.danduprolu.kiran.demo.blockchain.models.Blockchain;
import com.danduprolu.kiran.demo.blockchain.models.Transaction;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class BlockchainController {
    public static final int EMPTY_PROOF        = 0;
    public static final int DEFAULT_DIFFICULTY = 6;

    private BlockchainManager bcm;
    private ObjectMapper      json;

    public BlockchainController(@Autowired BlockchainManager bcm, @Autowired ObjectMapper json) {
        this.bcm = bcm;
        this.json = json;
        this.bcm.addBlockchain("default", new Blockchain());
    }

    @GetMapping("/chain")
    public String getDefaultBlockchain() throws JsonProcessingException {
        return getBlockchain("default");
    }

    @GetMapping("/chain/{id}")
    public String getBlockchain(@PathVariable String id) throws JsonProcessingException {
        return json.writeValueAsString(bcm.getBlockchain(id));
    }

    @PostMapping("/chain/{id}/block")
    public String mineNewBlock(@PathVariable String id) throws JsonProcessingException {
        Blockchain bc = bcm.getBlockchain(id);
        String previousHash = bc.size() == 0 ? "" : Blockchain.hash(bc.lastBlock());
        long proof = Blockchain.proofOfWork(bc.size() == 0 ? EMPTY_PROOF : bc.lastBlock().getProof(), DEFAULT_DIFFICULTY);

        Block b = bc.createBlock(proof, previousHash);
        return json.writeValueAsString(b);
    }

    @PostMapping("/chain/{id}/transaction")
    public String addNewTransaction(@PathVariable String id, @RequestBody Transaction t) throws JsonProcessingException {
        bcm.getBlockchain(id).addTransaction(t);
        return json.writeValueAsString(t);
    }
}

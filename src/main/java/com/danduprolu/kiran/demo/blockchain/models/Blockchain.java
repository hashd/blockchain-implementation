package com.danduprolu.kiran.demo.blockchain.models;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Blockchain implements Serializable {
    private List<Block>       blocks;
    private List<Transaction> transactions;

    public Blockchain() {
        this.blocks = new LinkedList<>();
        this.transactions = new LinkedList<>();
    }

    public static long proofOfWork(long lastProof, int difficulty) {
        long proof = 0;
        while (!isValidProof(proof, lastProof, difficulty)) {
            proof++;
        }

        return proof;
    }

    private static boolean isValidProof(long proof, long lastProof, int difficulty) {
        String code = "" + lastProof + proof;
        String hex = DigestUtils.sha256Hex(code);
        return hex.endsWith(String.join("", Collections.nCopies(difficulty, "0")));
    }

    public static String hash(Block block) {
        return DigestUtils.sha256Hex(block.toString());
    }

    public Block createBlock(long proof, String previousHash) {
        Block b = new Block(blocks.size(), new Date().getTime(), transactions, proof, previousHash);
        this.transactions = new LinkedList<>();
        this.blocks.add(b);
        return b;
    }

    public int createTransaction(String sender, String recipient, long amount) {
        this.transactions.add(new Transaction(sender, recipient, amount));
        return this.blocks.size();
    }

    public Block lastBlock() {
        return blocks.get(blocks.size() - 1);
    }

    public int addTransaction(Transaction t) {
        this.transactions.add(t);
        return this.blocks.size();
    }

    public int size() {
        return blocks.size();
    }
}

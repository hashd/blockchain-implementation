package com.danduprolu.kiran.demo.blockchain.components;

import com.danduprolu.kiran.demo.blockchain.models.Blockchain;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class BlockchainManager {
    private Map<String, Blockchain> blockchainMap = new HashMap<>();

    public void addBlockchain(String key, Blockchain bc) {
        blockchainMap.put(key, bc);
    }

    public Blockchain getBlockchain(String key) {
        return blockchainMap.get(key);
    }
}

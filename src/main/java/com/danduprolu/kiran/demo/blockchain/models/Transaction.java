package com.danduprolu.kiran.demo.blockchain.models;

import java.io.Serializable;

public class Transaction implements Serializable {
    private String sender;
    private String recipient;
    private long   amount;

    public Transaction() {
        
    }

    public Transaction(String sender, String recipient, long amount) {
        this.sender = sender;
        this.recipient = recipient;
        this.amount = amount;
    }
}

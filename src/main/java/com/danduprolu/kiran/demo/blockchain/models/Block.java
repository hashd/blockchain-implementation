package com.danduprolu.kiran.demo.blockchain.models;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;
import java.util.List;

public class Block implements Serializable {
    private int               index;
    private long              timestamp;
    private List<Transaction> transactions;
    private long              proof;
    private String            previousHash;

    public Block(int index, long timestamp, List<Transaction> transactions, long proof, String previousHash) {
        this.index = index;
        this.timestamp = timestamp;
        this.transactions = transactions;
        this.proof = proof;
        this.previousHash = previousHash;
    }

    void addTransaction(Transaction t) {
        this.transactions.add(t);
    }

    @Override
    public String toString() {
        ObjectMapper om = new ObjectMapper();
        try {
            return om.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public long getProof() {
        return proof;
    }
}
